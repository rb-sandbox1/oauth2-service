# OAuth2 Service

## Description
Demo OAuth2 Service for user registration and authentication. The service uses a stateless authentication system with JWT tokens to store necessary user data. Information about users and refresh tokens is stored in MongoDB. 

## Technologies used
- Java 17
- Spring Boot 2.7
- Spring Security
- Mongo DB
- Lombok
- Spring Boot Oauth2 resource server
- Docker compose
